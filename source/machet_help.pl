################################################################################
# Title      : Machet help text
# Project    : machet flow
###############################################################################
# File       : machet_help.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet sub script displaying help screen
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2010-12-29  Sascha Bannier   Created
# 2010-12-29  Sascha Bannier   Added Icarus Verilog help
# 2011-02-17  Sascha Bannier   Added Altera help,
#                              added tool override
# 2011-02-18  Sascha Bannier   Removed -list options
# 2011-05-04  Sascha Bannier   Added GHDL help
#                              Changed parameters to GNU style
# 2011-08-04  Sascha Bannier   Changed from --batch to --inter
#                              WAVE works with other options
#                              Removed --elaborate
#                              Added --version
# 2012-05-06  Sascha Bannier   Added ISim options
# 2012-07-18  Sascha Bannier   Added batch file write option
###############################################################################


use strict;


# |-----------------------------------------------------------------------------
# | Display help screen
# |-----------------------------------------------------------------------------
sub displayHelp {
  print "This tool reads a config file and creates all script files necessary for\n";
  print "simulation or FPGA synthesis, depending on the current working directory.\n";
#  print "  To perform simulation tasks, the script has to be started in\n";
#  print "    <project_base>/modules/<module_name>/sim\n";
#  print "\n";
#  print "  To perform FPGA related tasks, the script has to be started in\n";
#  print "    <project_base>/modules/<module_name>/fpga\n";
  print "\n";
  print "General options:\n";
  print "  -h, --help                 Display this help screen.\n";
  print "  -v, --version              Display version number.\n";
  print "  -f, --file <file_name>     Used to specify a different config file.\n";
  print "                             If this option is not used, the default file\n";
  print "                             \"machet.cfg\" will be used.\n";
  print "      --clean                Deletes all compile results and intermediate\n";
  print "                             files.\n";
  print "  -T, --tool <tool_chain>    Overrides toolchain specified in config file with\n";
  print "                             user selection.\n";
  print "  -V, --verbose              Display more information.\n";
  print "  -D, --dry                  Performs dry run. Scripts are created but not\n";
  print "                             started.\n";
  print "  -B, --batchfiles           Enables creation of batch files/shell script files\n";
  print "                             to start third-party tools.\n";
  print "\n";
  print "ModelSim related options:\n";
  print "  -c, --compile              Compiles all source files for simulation using\n";
  print "                             ModelSim VLOG or VCOM.\n";
  print "  -r, --run                  Starts ModelSim VSIM elaboration and simulation.\n";
  print "  -I, --inter                Runs simulation in interactive (GUI) mode.\n";
  print "                             Has only effect if \"--run\" is specified.\n";
  print "  -N, --nodump               VSIM will not dump signal values to a waveform\n";
  print "                             file. Has only effect if simulation is run in\n";
  print "                             non-interactive mode. \$dumpvars is not affected\n";
  print "                             by this option.\n";
  print "  -W, --wave                 Starts ModelSim VSIM to diplay the waveform of a\n";
  print "                             previous simulation.\n";
  print "  -L, --testlist             Displays a list of testcases.\n";
  print "  -C, --testcase <tc_name>   Selects testcase \"tc_name\".\n";
  print "\n";
  print "Icarus Verilog related options:\n";
  print "  -c, --compile              Compiles all source files for simulation using\n";
  print "                             IVERILOG.\n";
  print "  -r, --run                  Starts simulation.\n";
  print "  -N, --nodump               No value change dump is generated (i.e. \$dumpvars\n";
  print "                             is ignored).\n";
  print "  -W, --wave                 Starts GTKWAVE to diplay the waveform of a\n";
  print "                             previous simulation.\n";
  print "  -L, --testlist             Displays a list of testcases.\n";
  print "  -C, --testcase <tc_name>   Selects testcase \"tc_name\".\n";
  print "\n";
  print "GHDL related options:\n";
  print "  -c, --compile              Compiles and elaborates all source files for\n";
  print "                             simulation using GHDL.\n";
  print "  -r, --run                  Starts simulation.\n";
  print "  -N, --nodump               No waveform dump is generated\n";
  print "  -W, --wave                 Starts GTKWAVE to diplay the waveform of a\n";
  print "                             previous simulation.\n";
  print "\n";
  print "ISim related options:\n";
  print "  -c, --compile              Compiles source files for simulation using\n";
  print "                             Xilinx ISim.\n";
  print "  -r, --run                  Starts simulation.\n";
#  print "  -N, --nodump               No waveform dump is generated\n";
#  print "  -W, --wave                 Starts GTKWAVE to diplay the waveform of a\n";
  print "                             previous simulation.\n";
  print "\n";
  print "Xilinx FPGA related options:\n";
  print "  -s, --synthesis            Creates script files for FPGA synthesis and runs\n";
  print "                             Xilinx XST.\n";
  print "  -p, --par                  Creates script files for FPGA place and route and\n";
  print "                             runs Xilinx NGDBUILD, MAP, PAR and BITGEN.\n";
  print "  -t, --timing               Creates script files for FPGA timing analysis and\n";
  print "                             runs Xilinx TRACE.\n";
  print "\n";
  print "Altera FPGA related options:\n";
  print "  -s, --synthesis            Creates script files for FPGA synthesis and runs\n";
  print "                             Altera QUARTUS_MAP.\n";
  print "  -p, --par                  Creates script files for FPGA place and route and\n";
  print "                             runs Altera QUARTUS_FIT and QUARTUS_ASM.\n";
  print "  -t, --timing               Creates script files for FPGA timing analysis and\n";
  print "                             runs Altera QUARTUS_STA.\n";
}


# Dummy
1;
