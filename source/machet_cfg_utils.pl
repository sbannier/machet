###############################################################################
# Title      : Machet config file handling
# Project    : machet flow
###############################################################################
# File       : machet_cfg_utils.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet sub script for config file handling
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2010-12-29  Sascha Bannier   Created
# 2011-01-20  Sascha Bannier   Added support for Icarus Verilog
# 2011-02-09  Sascha Bannier   Added 'sim_tool' and 'fpga_tool' to template,
#                              added tool prefix to fpga and sim configurations
# 2011-02-09  Sascha Bannier   Added 'sim_tool' and 'fpga_tool'
# 2011-02-18  Sascha Bannier   Added Altera configuration part
# 2011-04-27  Sascha Bannier   Added createConfigFile subroutine
# 2011-05-02  Sascha Bannier   Changed order of entries in config file template
# 2011-05-04  Sascha Bannier   Added GHDL options to template
# 2011-07-08  Sascha Bannier   Split GHDl options to run and compile options
# 2012-05-06  Sascha Bannier   Added ISim compile options and added missing
#                              curly bracket for 'ghdl_compile_options'
###############################################################################


use strict;


# |-----------------------------------------------------------------------------
# | Extract data block from config file
# |-----------------------------------------------------------------------------
sub extractConfigInfo {
  my $VERBOSE = $MAIN::VERBOSE;             # Enable full comments

  my $content;
  my $pattern;
  my @patternArray = ();

  # Function accepts only two parameters
  unless (@_ == 2) {
    die("extractConfigInfo called with illegal number of arguments");
  }

  $content = $_[0];
  $pattern = $_[1];

  # Check if pattern is present
  while ($content =~ m/^\s*($pattern)\s*\{\s*((.|\s)*?)\}/gm) {
    # Remove empty lines, comments and spaces from found string
    my $pattern2 = $2;
    $pattern2 =~ s/^\s*?#.*//gm;
    $pattern2 =~ s/^\s*//gm;
    $pattern2 =~ s/\s*?$//gm;

    # Convert single string to array
    while ($pattern2 =~ m/(.+)/g) {
      push(@patternArray, $1);
    }
  }

  return @patternArray;
}



# |-----------------------------------------------------------------------------
# | Template for config file content
# |-----------------------------------------------------------------------------
sub createConfigTemplate {
  my $template = '';

  $template .= "implementation_name {\n";
  $template .= "  # Name of the implementation. Used to name all scripts and compile results\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "sim_tool {\n";
  $template .= "  # icarus\n";
  $template .= "  # modelsim\n";
  $template .= "  # ghdl\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_tool {\n";
  $template .= "  # xilinx\n";
  $template .= "  # altera\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "sim_top_level {\n";
  $template .= "  # Top level module name\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_top_level {\n";
  $template .= "  # Top level module name\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "source_paths {\n";
  $template .= "  # May point to directories or file, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "sim_source_paths {\n";
  $template .= "  # Source files that are used for simulation only\n";
  $template .= "  # May point to directories or file, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_source_paths {\n";
  $template .= "  # Source files that are used for FPGA implementation only\n";
  $template .= "  # May point to directories or file, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "include_paths {\n";
  $template .= "  # May point to directories only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "sim_excluded_files {\n";
  $template .= "  # May point to files only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_excluded_files {\n";
  $template .= "  # May point to files only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "verilog_defines {\n";
  $template .= "  # Global defines in Verilog syntax. Example:\n";
  $template .= "  # define1\n";
  $template .= "  # define2 123\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "sim_verilog_defines {\n";
  $template .= "  # Defines used for simulation only\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_verilog_defines {\n";
  $template .= "  # Defines used for FPGA implementation only\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_netlist_paths {\n";
  $template .= "  # List of directories containing pre-copmiled netlists\n";
  $template .= "  # May point to directories only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "fpga_part {\n";
  $template .= "  # Has to be in a format the FPGA toolchain understands\n";
  $template .= "  # Example:\n";
  $template .= "  # xc4vfx12-ff668-10\n";
  $template .= "  # xc3s200-ft256-4\n";
  $template .= "  # xc2v6000-ff1517-4\n";
  $template .= "  # ep3c16f484c6\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_syn_constraint_files {\n";
  $template .= "  # .xcf and .ucf files for synthesis part of Xilinx FPGA tools\n";
  $template .= "  # May point to files only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_par_constraint_files {\n";
  $template .= "  # .ucf files for place and route part of Xilinx FPGA tools\n";
  $template .= "  # May point to files only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_xst_options {\n";
  $template .= "  # Any option that XST understands\n";
  $template .= "  -rtlview yes\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_ngdbuild_options {\n";
  $template .= "  # Any option that NGDBUILD understands\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_map_options {\n";
  $template .= "  # Any option that MAP understands\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_par_options {\n";
  $template .= "  # Any option that PAR understands\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_bitgen_options {\n";
  $template .= "  # Any option that BITGEN understands\n";
  $template .= "  -g StartUpClk:JtagClk\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "xilinx_trace_options {\n";
  $template .= "  # Any option that TRCE understands\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "altera_constraint_files {\n";
  $template .= "  # .tcl files for synthesis part of Altera FPGA tools\n";
  $template .= "  # File content will be added to Altera project file\n";
  $template .= "  # May point to files only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "altera_sdc_files {\n";
  $template .= "  # .sdc files used by TimeQuest timing analyzer\n";
  $template .= "  # May point to files only, relative to project base directory\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "modelsim_vlog_options {\n";
  $template .= "  # Any option that ModelSim vlog understands\n";
  $template .= "  #-timescale \"1ns/1ns\"\n";
  $template .= "  -novopt\n";
  $template .= "  -source\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "modelsim_vcom_options {\n";
  $template .= "  # Any option that ModelSim vcom understands\n";
  $template .= "  -novopt\n";
  $template .= "  -source\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "modelsim_vsim_options {\n";
  $template .= "  # Any option that ModelSim vsim understands\n";
  $template .= "  -novopt\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "icarus_iverilog_options {\n";
  $template .= "  # Any option that Icarus iverilog compiler understands\n";
  $template .= "  -Wall\n";
  $template .= "  -Wno-timescale\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "icarus_vvp_options {\n";
  $template .= "  # Any option that Icarus vvp simulator understands\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "ghdl_compile_options {\n";
  $template .= "  # Any option that the GHDL compiler understands\n";
  $template .= "  #--ieee=synopsys\n";
  $template .= "  #-fexplicit\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "ghdl_run_options {\n";
  $template .= "  # Any option that a GHDL simulation executable understands\n";
  $template .= "  #--stop-time=1ms\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "isim_compile_options {\n";
  $template .= "  # Any option that the ISim compiler (fuse) understands\n";
  $template .= "  --incremental\n";
  $template .= "}\n";
  $template .= "\n";
  $template .= "isim_run_options {\n";
  $template .= "  # Any options that a ISim simulation executable understands\n";
  $template .= "  --incremental\n";
  $template .= "}\n";

  return ($template);
}



# |-----------------------------------------------------------------------------
# | Write config file
# |-----------------------------------------------------------------------------
sub createConfigFile {

  my $fileloc;
  my $question = '';

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("createConfigFile called with illegal number of arguments");
  }

  # Store path
  $fileloc = $_[0];

  # Wait for user input
  printf ("Warning: Config file \n  \"%s\"\ndoes not exist. Create new file from template? (y/n).\n", $fileloc);
  $question = <STDIN>;
  chomp($question);

  # Write file
  if ($question eq "y") {
    open(CFGFILE, "> $fileloc") or die "Could not create config file!";
    print CFGFILE createConfigTemplate();
    close(CFGFILE);
  }
}





# Dummy
1;
