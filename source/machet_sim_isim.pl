###############################################################################
# Title      : Machet ISim flow
# Project    : machet flow
###############################################################################
# File       : machet_sim_isim.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet sub script for Xilinx ISim simulation
###############################################################################
# Copyright (c) 2012   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2012-05-04  Sascha Bannier   Created
###############################################################################


use strict;


sub sim_isim {

  my $VERBOSE           = $MAIN::VERBOSE;             # Enable full comments

  my $DRY               = $MAIN::DRY;                 # Perform dry run only, creates only scripts
  my $TESTCASE          = $MAIN::TESTCASE;            # Name of selected testcase
  my $COM               = $MAIN::COM;                 # Command line option for simulation compile run
  my $RUN               = $MAIN::RUN;                 # Command line option for simulation start
  my $INTER             = $MAIN::INTER;               # Command line option for interactive (GUI) simulation
  my $NODUMP            = $MAIN::NODUMP;              # Command line option to disable value change dumps in simulation
#  my $WAVE              = $MAIN::WAVE;                # Command line option to display waveform

  my $CALLDIR           = $MAIN::CALLDIR;             # Directory from where this script is called
  my @IMP_NAME          = @MAIN::IMP_NAME;            # Implementation name
  my @INCLUDE_PATHS     = @MAIN::INCLUDE_PATHS;       # List of include files (pointing to files directly)
  my @VERILOG_DEFINES   = @MAIN::VERILOG_DEFINES;     # Verilog defines used for all files
  my @TOPLEVEL          = @MAIN::TOPLEVEL;            # Top level module name for FPGA or SIM run

  my @SOURCEFILE_LIST   = @MAIN::SOURCEFILE_LIST;     # Complete list of all source files with absolute path

  my @FUSEOPTIONS       = @MAIN::ISIM_COMPILEOPTIONS; # Options to parse to ISim compiler
  my @RUNOPTIONS        = @MAIN::ISIM_RUNOPTIONS;     # Options to parse to ISim simulator

  # Local variables
  my $ISIMPRJ_FILE = '';         # Content of .prj file for ISim
  my $ISIM_COMPILECFG_FILE = ''; # Configuration file for ISim run
  my $ISIM_RUNCFG_FILE = '';     # Configuration file for ISim run
  my $COMPILE_COMMAND;           # Command to start ISIM compiler
  my $RUN_COMMAND = '';          # Command to start simulation
#  my $WAVE_COMMAND = '';         # Command to display waveform

  my $logfileName;               # Name of logfile, imp, name expanded with testcase name
  my $fileloc;                   # Location of file
  my $line;                      # Single line
  my $currentEntry;              # Current entry from list
  my $ret = 1;                   # Return value

  # Create simulation directory
  unless (-d convPath($CALLDIR."/isim")) {
    systemCall("mkdir isim");
  }


  # Add testcase path to include paths
  if ($TESTCASE ne "") {
    $currentEntry = cleanPath($CALLDIR."/../testcases/".$TESTCASE);
    # Check if testcase directory exists and append to include path list
    if (-d convPath($currentEntry)) {
      push(@INCLUDE_PATHS, $currentEntry);
    }
    # Else terminate run
    else {
      print "ERROR: Testcase \"$TESTCASE\" does not exist\n";
      return(-1);
    }
  }

  # ----------------------------------------------------------------------------
  # Build simulation compile batch files
  if ($COM == 1) {

    print "/-----------------------------------------------------------------------------\\\n";
    print "| Building compile script files                                               |\n";
    print "\\-----------------------------------------------------------------------------/\n";

    # Building compiler configuration file
    # Toplevel
    $ISIM_COMPILECFG_FILE = "-t ".$TOPLEVEL[0]."\n\n";
    # Output file name
    $ISIM_COMPILECFG_FILE .= "-o ".$IMP_NAME[0]."\n\n";
    # ISim project file
    $ISIM_COMPILECFG_FILE .= "--prj ../".$IMP_NAME[0].".prj\n\n";
    # Include paths
    foreach $currentEntry (@INCLUDE_PATHS) {
      $ISIM_COMPILECFG_FILE .= "-i ".$currentEntry."\n";
    }
    $ISIM_COMPILECFG_FILE .= "\n";
    # Defines
    foreach $currentEntry (@VERILOG_DEFINES) {
      $currentEntry =~ s/^(\w+?)[ ]+(.+)/$1="$2"/;
      # Append to string
      $ISIM_COMPILECFG_FILE .= "-d ".$currentEntry."\n";
    }
    $ISIM_COMPILECFG_FILE .= "\n";
    # Options from config file
    foreach $currentEntry (@FUSEOPTIONS) {
      $ISIM_COMPILECFG_FILE .= $currentEntry."\n";
    }

    # Write config file
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".isim.com.cfg");
    open(CFGFILE, ">", $fileloc) or die "Could not open ISim compiler config file!";
    print CFGFILE $ISIM_COMPILECFG_FILE;
    close(CFGFILE);
    #Report
    if ($VERBOSE == 1) {
      printf ("  Wrote \"%s\"\n", $fileloc);
    }


    # Check file type of all source files in list. Remove every file from
    # beginning of list, check if file type is supported, add entry to compiler
    # config file and put file back to end of source file list. Not supported
    # file types are removed from list
    foreach (1..@SOURCEFILE_LIST) {
      # Remove first entry from source file list
      $line = shift(@SOURCEFILE_LIST);

      # Verilog files
      if ($line =~ m/\.v$/i) {
        push(@SOURCEFILE_LIST, $line);
        $line = 'verilog work "'.$line.'"'."\n";
        $ISIMPRJ_FILE .= $line;
      }
      # VHDL files
      elsif ($line =~ m/\.vhd$/i) {
        push(@SOURCEFILE_LIST, $line);
        $line = 'vhdl work "'.$line.'"'."\n";
        $ISIMPRJ_FILE .= $line;
      }
      # Something
      else {
        printf ("Warning: \"%s\" is unsupported file type\n", convPath($line));
      }
    }

    # Write .prj file
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".prj");
    open(PRJFILE, "> $fileloc") or die "Could not write ISim project file!";
    print PRJFILE $ISIMPRJ_FILE;
    close(PRJFILE);


    # Compile command
    $COMPILE_COMMAND = "fuse -f ../".$IMP_NAME[0].".isim.com.cfg";

    # Write batch files
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".com");
    writeBatch($fileloc, "isim", $COMPILE_COMMAND);
  }



  # ----------------------------------------------------------------------------
  # Build simulation run batch file
  if ($RUN == 1) {
    print "/-----------------------------------------------------------------------------\\\n";
    print "| Building simulation script files                                            |\n";
    print "\\-----------------------------------------------------------------------------/\n";

    # Expand logfile name with testcase name
    $logfileName = $IMP_NAME[0];
    unless ($TESTCASE eq "") {
      $logfileName .= ".".$TESTCASE;
    }
    $logfileName .= ".log";

    # Building simulator configuration file
    # Log file name
    $ISIM_RUNCFG_FILE .= " -log ".$logfileName."\n";
    # Choose tcl file to run
    if ($INTER == 1) {
      $ISIM_RUNCFG_FILE .= " -tclbatch ../".$IMP_NAME[0].".inter.tcl\n";
    }
    elsif ($NODUMP == 1) {
      $ISIM_RUNCFG_FILE .= " -tclbatch ../".$IMP_NAME[0].".nodump.tcl\n";
    }
    else {
      $ISIM_RUNCFG_FILE .= " -tclbatch ../".$IMP_NAME[0].".batch.tcl\n";
    }
    # Waveform file
    # TODO: works in Xilinx ISE 10 tools, ISE 11 does not support this option
    $ISIM_RUNCFG_FILE .= " -wavefile ".$IMP_NAME[0].".xwv\n";

    # Write config file
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".isim.run.cfg");
    open(CFGFILE, ">", $fileloc) or die "Could not open ISim simulator config file!";
    print CFGFILE $ISIM_RUNCFG_FILE;
    close(CFGFILE);
    #Report
    if ($VERBOSE == 1) {
      printf ("  Wrote \"%s\"\n", $fileloc);
    }


    # Create tcl files if they are not present
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".nodump.tcl");
    createTclFile($fileloc, "nodump");

    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".batch.tcl");
    createTclFile($fileloc, "batch");

    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".inter.tcl");
    createTclFile($fileloc, "inter");


    # Run command
    $RUN_COMMAND .= "./".$IMP_NAME[0]." -f ../".$IMP_NAME[0].".isim.run.cfg";
    # TODO this is the windows command
#    $RUN_COMMAND .= $IMP_NAME[0].".exe -f ../".$IMP_NAME[0].".isim.run.cfg";
    # TODO does not work until XILINX ISE 11
#    if ($INTER == 1) {
#      $RUN_COMMAND .= " -gui";
#    }

    # Write batch files
    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".run");
    writeBatch($fileloc, "isim", $RUN_COMMAND);
  }


#
#  if ($WAVE == 1) {
#    print "/-----------------------------------------------------------------------------\\\n";
#    print "| Building waveform script files                                              |\n";
#    print "\\-----------------------------------------------------------------------------/\n";
#    # Command for wave display
#    $WAVE_COMMAND = "";
#
#    # Write batch file
#    $fileloc = convPath($CALLDIR."/".$IMP_NAME[0].".wave");
#    writeBatch($fileloc, "isim", $WAVE_COMMAND);
#  }





  # Start batch files
  unless ($DRY == 1) {

    if ($COM == 1) {
      print "/-----------------------------------------------------------------------------\\\n";
      print "| Starting ISim compiler                                                      |\n";
      print "\\-----------------------------------------------------------------------------/\n";

      # Report
      if ($TESTCASE ne "") {
        print "  Compiling testcase \"$TESTCASE\"\n";
      }

      # Start compile and stop on errors
      chdir("isim");
      $ret = systemCall($COMPILE_COMMAND);
      chdir("..");
      if ($ret != 0) {
        return($ret);
      }

    }

    if ($RUN == 1) {
      print "/-----------------------------------------------------------------------------\\\n";
      print "| Starting simulation                                                         |\n";
      print "\\-----------------------------------------------------------------------------/\n";

      # Start simulation
      chdir("isim");
      $ret = systemCall($RUN_COMMAND);
      chdir("..");
      if ($ret != 0) {
        return($ret);
      }
    }

#    if ($WAVE == 1) {
#      print "/-----------------------------------------------------------------------------\\\n";
#      print "| Displaying waveform                                                         |\n";
#      print "\\-----------------------------------------------------------------------------/\n";
#
#      # Start waveform viewer
#      chdir("isim");
#      $ret = systemCall($WAVE_COMMAND);
#      chdir("..");
#      if ($ret != 0) {
#        return($ret);
#      }
#    }
  }

  return($ret);

}






# |-----------------------------------------------------------------------------
# | Template for ISim tcl file content
# |-----------------------------------------------------------------------------
sub createTclTemplate {
  my $template = '';

  unless (@_ == 1) {
    die("createTclTemplate called with illegal number of arguments");
  }

  # Create tcl file for batch simulation without dump
  if ($_[0] eq "nodump") {
    $template .= "run all\n";
    $template .= "exit\n";
  }
  # Create tcl file for batch simulation
  elsif ($_[0] eq "batch") {
    $template .= "ntrace select -m / -l all\n";
    $template .= "ntrace start\n";
    $template .= "run all\n";
    $template .= "ntrace stop\n";
    $template .= "exit\n";
  }
  # Create tcl file for interactive simulation
  elsif ($_[0] eq "inter") {
  }
  # Anything else results in error
  else {
    die("createTclTemplate called with illegal argument value");
  }
  return ($template);
}



# |-----------------------------------------------------------------------------
# | ISim tcl file creation
# |-----------------------------------------------------------------------------
sub createTclFile {
  my $VERBOSE           = $MAIN::VERBOSE;          # Enable full comments
  my $tclFile;
  my $tclMode;

  unless (@_ == 2) {
    die("createTclTemplate called with illegal number of arguments");
  }

  $tclFile = $_[0];
  $tclMode = $_[1];

  unless (-f $tclFile) {
    open(TCLFILE, ">", "$tclFile") or die "Could not open tcl file!";
    print TCLFILE createTclTemplate($tclMode);
    close(TCLFILE);
    #Report
    if ($VERBOSE == 1) {
      printf ("  Wrote \"%s\"\n", $tclFile);
    }
  }
  else {
    #Report
    if ($VERBOSE == 1) {
      printf ("  \"%s\" already exists\n", $tclFile);
    }
  }
}





# Dummy
1;
