###############################################################################
# Title      : Machet Utilities
# Project    : machet flow
###############################################################################
# File       : machet_utils.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet sub script containing utility sub routines
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2010-12-29  Sascha Bannier   Created
# 2011-02-11  Sascha Bannier   Added writeBatch subroutine
# 2011-02-23  Sascha Bannier   Added systemCall subroutine
# 2011-04-20  Sascha Bannier   Added expandToAbsolutePath subroutine
# 2011-04-27  Sascha Bannier   Moved parameter count check to beginning of
#                              subroutine
# 2011-10-07  Sascha Bannier   fixed 'cleanPath' routine which failed for
#                              directories containing '-' character
# 2012-07-18  Sascha Bannier   Batch files are only written if command line
#                              option is set
###############################################################################


use strict;


# |-----------------------------------------------------------------------------
# | Removes "/XXX/.." from path
# |-----------------------------------------------------------------------------
sub cleanPath {
  my $path;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("cleanPath called with illegal number of arguments");
  }

  # Store path
  $path = $_[0];

  # Find "/XXX/.." and remove it from string
  while ($path =~ m|/[-\w]+?/\.\.|) {
    $path =~ s|/[-\w]+?/\.\.||i;
  }

  return $path;
}



# |-----------------------------------------------------------------------------
# | Check if a path is an absolute or relative path
# |-----------------------------------------------------------------------------
sub checkAbsolutePath {
  my $OS = $MAIN::OS;
  my $path;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("checkAbsolutePath called with illegal number of arguments");
  }

  # Store path
  $path = $_[0];

  if ($OS eq "WIN") {
    # Check if path begins with drive letter
    if ($path =~ m/^[a-zA-Z]:/) {
      return (1);
    }
    else {
      return (0);
    }
  }
  else {
    # Check if path begins with "/"
    if ($path =~ m/^\//) {
      return (1);
    }
    else {
      return (0);
    }
  }
}



# |-----------------------------------------------------------------------------
# | Expands a path with project base directory if path does not point to an
# | absolute location
# |-----------------------------------------------------------------------------
sub expandToAbsolutePath {
  my $PROJECTBASE = $MAIN::PROJECTBASE;
  my $path;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("expandToAbsolutePath called with illegal number of arguments");
  }

  # Store path
  $path = $_[0];

  # Check if path is absolute or relative
  unless (checkAbsolutePath($path)) {
    # Expand to complete path
    $path = $PROJECTBASE."/".$path;
  }

  return $path;
}



# |-----------------------------------------------------------------------------
# | Convert path style to Selected OS
# |-----------------------------------------------------------------------------
sub convPath {
  my $OS = $MAIN::OS;
  my $path;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("convPath called with illegal number of arguments");
  }

  # Store path
  $path = $_[0];

  if ($OS eq "WIN") {
    return convPathToWin($path);
  }
  else {
    return convPathToUnix($path);
  }
}



# |-----------------------------------------------------------------------------
# | Convert UNIX path style to WINDOWS style
# |  Replaces "/" with "\" in given string "@_"
# |-----------------------------------------------------------------------------
sub convPathToWin {
  my $path;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("convPathToWin called with illegal number of arguments");
  }

  # Store UNIX path
  $path = $_[0];

  $path =~ s/\//\\/gm;

  return $path;
}



# |-----------------------------------------------------------------------------
# | Convert WINDOWS path style to UNIX style
# |  Replaces "\" with "/" in given string "@_"
# |-----------------------------------------------------------------------------
sub convPathToUnix {
  my $path;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("convPathToUnix called with illegal number of arguments");
  }

  # Store WINDOWS path
  $path = $_[0];

  $path =~ s/\\/\//gm;

  return $path;
}



# |-----------------------------------------------------------------------------
# | Wrapper for system() command
# |  returns a usable exit code (system returns code from executed command and
# |  from system command (i.e. sh, etc.) itself)
# |-----------------------------------------------------------------------------
sub systemCall {
  my $command;
  my $ret;

  # Function accepts only one parameter
  unless (@_ == 1) {
    die("systemCall called with illegal number of arguments");
  }

  # Store command
  $command = $_[0];

  $ret = system("$command");

  $ret = ($ret >> 8) | ($ret & 0xFF);

  return($ret);

}



# |-----------------------------------------------------------------------------
# | Creates shell script and batch file
# |  Parameters:
# |   1. location and name of file to create, file ending will be appended
# |      automatically
# |   2. directory in which the command will be started
# |   3. command to run
# |-----------------------------------------------------------------------------
sub writeBatch {

  my $BATCH   = $MAIN::BATCHFILES;
  my $VERBOSE = $MAIN::VERBOSE;

  my $location;
  my $dir;
  my $command;

  # Function accepts only three parameters
  unless (@_ == 3) {
    die("genBatch called with illegal number of arguments");
  }

  # Return if batch file generation is not enabled
  unless ($BATCH == 1) {
    return;
  }

  $location = $_[0];
  $dir = $_[1];
  $command = $_[2];

  # Create Unix sh script
  open(SHFILE, ">", "$location.sh") or die "Could not write sh script file!";
  print SHFILE "#! /bin/sh\ncd ".$dir."\n".$command."\ncd ..\n";
  close(SHFILE);
  #Report
  if ($VERBOSE == 1) {
    printf ("  Wrote \"%s.sh\"\n", $location);
  }

  # Create Windows batch file
  open(BATFILE, ">", "$location.bat") or die "Could not write batch file!";
  print BATFILE "cd ".$dir."\n".$command."\ncd ..\n";
  close(BATFILE);
  #Report
  if ($VERBOSE == 1) {
    printf ("  Wrote \"%s.bat\"\n", $location);
  }
}




# Dummy
1;
