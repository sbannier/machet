#! /usr/bin/perl -w
###############################################################################
# Title      : 
# Project    : 
###############################################################################
# File       : gen_hw_module.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Script to create a module directory skeleton
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2010-01-07  Sascha Bannier   Created
# 2011-02-21  Sascha Bannier   Added tb directory, checking if directories
#                              already exist
# 2011-02-27  Sascha Bannier   Added operating system selector
# 2013-01-23  Sascha Bannier   Removed operating system dependency
###############################################################################


use strict;
use warnings;
use Cwd;


# Variable declaration
my @dirs = ("doc",
            "fpga",
            "fpga/constraints",
            "fpga/netlists",
            "include",
            "sim",
            "src",
            "tb",
            "testcases"
           );

# Modules to create
my @moduleNames;


print "/-----------------------------------------------------------------------------\\\n";
print "| GEN_HW_MODULE 0.1.4                                                         |\n";
print "\\-----------------------------------------------------------------------------/\n";

my $callDir = cwd();

# Use specified arguments as module name
if (@ARGV >= 1) {
  @moduleNames = @ARGV;
}
else {
  print "Error: Wrong number of arguments for gen_module.pl\n";
  exit();
}

# Step through module list
foreach my $currentModule (@moduleNames) {
  # Create module
  unless (-d "$currentModule") {
    mkdir($currentModule);
  }
  # Create all sub-directories in list
  foreach my $currentDir (@dirs) {
    # Build complete path
    my $createDir = $callDir."/".$currentModule."/".$currentDir;

    unless (-d "$createDir") {
      mkdir($createDir);
    }
  }
  print "  Created module $currentModule\n";
}


