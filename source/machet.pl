#! /usr/bin/perl -w
###############################################################################
# Title      : Machet script
# Project    : machet flow
###############################################################################
# File       : machet.pl
# Author     : Sascha Bannier
# Company    : ---
###############################################################################
# Description: Machet design flow script
###############################################################################
# Copyright (c) 2011   Sascha Bannier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Revisions  :
# Date        Author           Description
# 2010-01-08  Sascha Bannier   Created
# 2011-01-01  Sascha Bannier   First version with mudular script files
# 2011-01-20  Sascha Bannier   Added support for Icarus Verilog,
#                              clean works under non-Windows OS
# 2011-01-21  Sascha Bannier   Added -nodump option for simulation
# 2011-01-30  Sascha Bannier   Removed /../ from source file paths
# 2011-02-09  Sascha Bannier   Added tool selection to config file,
#                              added tool specific prefix to variable names,
#                              tool selection works
# 2011-02-11  Sascha Bannier   Added Altera support
# 2011-02-17  Sascha Bannier   Added tool override,
#                              -file uses relative path from call dir
# 2011-02-18  Sascha Bannier   Removed $LIST from config file processing,
#                              added Altera config extraction
# 2011-02-21  Sascha Bannier   Removed $LIST completely
# 2011-02-23  Sascha Bannier   Added exit codes
# 2011-02-27  Sascha Bannier   Added operating system selector
# 2011-04-20  Sascha Bannier   Added subroutine to expand path to absolute
#                              loaction
# 2011-04-27  Sascha Bannier   Moved config file generation to subroutine
#                              Changed version to 0.3.0
# 2011-05-03  Sascha Bannier   Added support for GHDL
# 2011-05-04  Sascha Bannier   GHDL options read from config file
#                              Changed parameters to GNU style
# 2011-05-05  Sascha Bannier   Fixed bug where -C was used for compile and
#                              testcase selection
# 2011-07-08  Sascha Bannier   Split GHDL options to run and compile options
# 2011-07-09  Sascha Bannier   Getopt used for command line option parsing
# 2011-07-21  Sascha Bannier   Made file locations configurable
# 2011-08-04  Sascha Bannier   Changed command option handling
#                              Project paths are now stored in
#                              machet_config.pl
#                              Added --version
#                              Fixed Perl warnings
# 2011-08-08  Sascha Bannier   Changed typo (pathes to paths, how embarrassing)
# 2012-05-06  Sascha Bannier   Added Xilinx ISim support
# 2012-05-08  Sascha Bannier   Fixed bug regarding absolute path for cfg file
#                              override
# 2012-07-18  Sascha Bannier   Added batch file write option
# 2013-01-23  Sascha Bannier   Clean operation simplified
###############################################################################



package MAIN;



use strict;
use warnings;

# Used to get current working directory
use Cwd;


# Used for command line option parsing
use Getopt::Long;
Getopt::Long::Configure('posix_default', 'no_ignore_case');

# Used to delete subdirs during clean
use File::Path;

# Select operating system version, standard in non-Windows
our $OS = "X";
if ($^O =~ m/^MSWin/) {
  $OS = "WIN";
}


# Version number
my $VERSION = "0.5.2 --ISim test--";


# Add current path to INC search path
use FindBin;                  # Locate this script
use lib "$FindBin::Bin";      # Use script dir


# Require all script files
# Configuration settings
require "machet_config.pl";
sub getMode;                  # Returns mode
sub getProjectBase;           # Returns project base directory
sub getDefaultCfgLocation;    # Returns location of default config file

# Help display
require "machet_help.pl";
sub displayHelp;              # Prints the help screen

# Config file handling
require "machet_cfg_utils.pl";
sub extractConfigInfo;        # Extract info from config files
sub createConfigTemplate;     # Returns a config file template
sub createConfigFile;         # Write config file template to disk

# Collection of small helper functions
require "machet_utils.pl";
sub checkAbsolutePath;        # Check if a path is absolute or relative. Returns 1 if path is absolute
sub expandToAbsolutePath;     # Expand path to absolute path loaction
sub cleanPath;                # Cleans all "/XXX/../" from path
sub convPath;                 # Convert path to OS selcted by $OS
sub convPathToWin;            # Convert UNIX path to WINDOWS format
sub convPathToUnix;           # Convert WINDOWS path to UNIX format
sub systemCall;               # Wrapper for system(), processes exit code to be useful
sub writeBatch;               # Tool batch file generation

# FPGA synthesis for XILINX
require "machet_fpga_xilinx.pl";
sub fpga_xilinx;              # Performs FPGA synthesis using Xilinx tools

# FPGA synthesis for XILINX
require "machet_fpga_altera.pl";
sub fpga_altera;              # Performs FPGA synthesis using Altera tools

# ModelSim simulation
require "machet_sim_modelsim.pl";
sub sim_modelsim;             # Start simulation using ModelSim
sub createDoTemplate;         # Returns a do file template
sub createDoFile;             # Creates a do file

# ISim simulation
require "machet_sim_isim.pl";
sub sim_isim;                 # Start simulation using ModelSim

# Icarus Verilog simulation
require "machet_sim_icarus.pl";
sub sim_icarus;               # Start simulation using Icarus Verilog

# GHDL simulation
require "machet_sim_ghdl.pl";
sub sim_ghdl;                 # Start simulation using GHDL


# Sub prototypes
sub cleanUp;                  # Cleans all compile results
sub displayTestList;          # Display all testcases

# Variable declaration
our $MODE;                    # Selected mode (SIM ord FPGA)
our $HELP = 0;                # Display help screen
our $BATCHFILES = 0;          # Write batch files for third party tools
our $VER = 0;                 # Show version
our $VERBOSE = 0;             # Enable full comments
our $TOOL_OVERRIDE = '';      # Override tool from config file
our @CFG_OVERRIDE = ();       # Override standard config file
our $CLEAN = 0;               # Command line option to clean up
our $CLEANONLY = 0;           # Set if only clean has been selected from command line
our $DRY = 0;                 # Perform dry run only, creates only scripts

our $SYN_OPT = 0;             # Command line option for synthesis is set
our $PAR_OPT = 0;             # Command line option for place and route is set
our $TIMING_OPT = 0;          # Command line option for timing analysis is set
our $SYN = 0;                 # Flag for synthesis is set
our $PAR = 0;                 # Flag for place and route is set
our $TIMING = 0;              # Flag for timing analysis is set

our $TESTLIST_OPT = 0;        # Command line option for displaying testcase list
our $TESTCASE_OPT = '';       # Name of selected testcase from command line
our $COM_OPT = 0;             # Command line option for simulation compile run
our $RUN_OPT = 0;             # Command line option for simulation start
our $INTER_OPT = 0;           # Command line option for interactive simulation
our $NODUMP_OPT = 0;          # Command line option to disable value change dumps in simulation
our $WAVE_OPT = 0;            # Command line option to display waveform
our $TESTLIST = 0;            # Flag for displaying testcase list
our $TESTCASE = '';           # Name of selected testcase
our $COM = 0;                 # Flag for simulation compile run
our $ELAB = 0;                # Flag for simulation elaboration
our $RUN = 0;                 # Flag for simulation start
our $INTER = 0;               # Flag for interactive simulation
our $NODUMP = 0;              # Flag to disable value change dumps in simulation
our $WAVE = 0;                # Flag to display waveform

our $CALLDIR;                 # Directory from where this script is called
our $PROJECTBASE;             # Base directory of project
my  @CFGNAME = ();            # Name of config file
our @TOOL;                    # Used toolchain
our @IMP_NAME;                # Implementation name
our @SOURCE_PATHS;            # List of source file locations (pointing to file or directory)
our @ADDSOURCE_PATHS;         # List of additional source paths for SIM or FPGA
our @INCLUDE_PATHS;           # List of include paths
our @VERILOG_DEFINES;         # Verilog defines used for all files
our @ADDVERILOG_DEFINES;      # List of additional defines for SIM or FPGA
our @EXCLUDED_FILES;          # Files excluded for FPGA or SIM run (pointing to files directly)
our @TOPLEVEL;                # Top level module name for FPGA or SIM run
our @FPGAPART;                # Part type of target FPGA
our @FPGA_NETLIST_PATHS;      # Path information for pre-compiled netlists
our @XIL_SYNCONSTRAINT_FILES; # List of synthesis constraint files for FPGA run (pointing to files directly)
our @XIL_PARCONSTRAINT_FILES; # List of par constraint files for FPGA run (pointing to files directly)
our @XIL_XSTOPTIONS;          # Options to parse to XST during synthesis
our @XIL_NGDBUILDOPTIONS;     # Options to parse to NGDBUILD during mapping
our @XIL_MAPOPTIONS;          # Options to parse to MAP during mapping
our @XIL_PAROPTIONS;          # Options to parse to PAR during mapping
our @XIL_BITGENOPTIONS;       # Options to parse to BITGEN
our @XIL_TRACEOPTIONS;        # Options to parse to TRACE
our @ALT_CONSTRAINT_FILES;    # List of constraints files for Altera FPGA flow
our @ALT_SDC_FILES;           # List of constraints files for Altera timing analysis
our @MSIM_VLOGOPTIONS;        # Options to parse to ModelSim vlog compilers
our @MSIM_VCOMOPTIONS;        # Options to parse to ModelSim vcom compilers
our @MSIM_VSIMOPTIONS;        # Options to parse to ModelSim vsim simulator
our @ICAR_IVERILOGOPTIONS;    # Options to parse to IVERILOG compiler
our @ICAR_VVPOPTIONS;         # Options to parse to VVP simulator
our @GHDL_COMOPTIONS;         # Options to parse to GHDL compiler
our @GHDL_RUNOPTIONS;         # Options to parse to GHDL for simulation run
our @ISIM_COMPILEOPTIONS;     # Options to parse to ISim compiler
our @ISIM_RUNOPTIONS;         # Options to parse to ISim simulator

our @SOURCEFILE_LIST;         # Complete list of all source files with absolute path


my $line;                     # Single line
my $cfgContent;               # Content of config file
my $currentEntry;             # Current entry in list read from config file
my $currentPath;              # Current path that is evaluated

my $ret = 1;                  # Return code of toolchain

# ------------------------------------------------------------------------------
# First step: evaluate command line options, get directory in which script is
# started and check for mode (FPGA/SIM)

print "/-----------------------------------------------------------------------------\\\n";
print "| MACHET Digital Design Flow $VERSION";
for (30..(78 - length($VERSION))) {
  print " ";
}
print "|\n";
print "\\-----------------------------------------------------------------------------/\n";

# Evaluate arguments
unless (GetOptions('help|h'       => \$HELP,
                   'batchfiles|B' => \$BATCHFILES,
                   'version|v'    => \$VER,
                   'verbose|V'    => \$VERBOSE,
                   'clean'        => \$CLEAN,
                   'dry|D'        => \$DRY,
                   'file|f=s'     => \@CFG_OVERRIDE,
                   'tool|T=s'     => \$TOOL_OVERRIDE,
                   'synthesis|s'  => \$SYN_OPT,
                   'par|p'        => \$PAR_OPT,
                   'timing|t'     => \$TIMING_OPT,
                   'testlist|L'   => \$TESTLIST_OPT,
                   'testcase|C=s' => \$TESTCASE_OPT,
                   'compile|c'    => \$COM_OPT,
                   'run|r'        => \$RUN_OPT,
                   'inter|I'      => \$INTER_OPT,
                   'nodump|N'     => \$NODUMP_OPT,
                   'wave|W'       => \$WAVE_OPT
                  ) == 1) {
  exit(-1);
}
unless (@ARGV == 0) {
  print "Error processing option: ".$ARGV[0]."\n";
  exit(-1);
}

# Do nothing if --version has been chosen
if ($VER == 1) {
  print "Copyright (C) 2010  Sascha Bannier\n";
  print "This program comes with ABSOLUTELY NO WARRANTY;\n";
  print "This is free software, and you are welcome to redistribute it\n";
  print "under certain conditions; See file gpl.txt for details.\n";
  exit(0);
}
# Check if help argument was specified
if ($HELP == 1) {
  displayHelp();
  exit(0);
}

# Get directory
$CALLDIR = cwd();
# Check which mode is selected based on current working directory
$MODE = getMode();
# Get project base directory
$PROJECTBASE = getProjectBase();
# Get name and location of config file
if ((!defined $CFG_OVERRIDE[0])) {
  @CFGNAME = convPath(getDefaultCfgLocation());
}
else {
  foreach (@CFG_OVERRIDE) {
    # Check if override path is absolute or relative
    if (checkAbsolutePath($_)) {
      push(@CFGNAME, convPath(cleanPath($_)));
    }
    else {
      push(@CFGNAME, convPath(cleanPath($CALLDIR."/".$_)));
    }
  }
}



# Set/reset options based on combination of options
# For FPGA mode
if ($MODE eq "FPGA") {
  $SYN    = $SYN_OPT;
  $PAR    = $PAR_OPT;
  $TIMING = $TIMING_OPT;

  # Check if any FPGA options have been specified
  if (($SYN == 0) && ($PAR == 0) && ($TIMING == 0)) {
    # Clean has been specified so just do a cleanup
    if ($CLEAN == 1) {
      $CLEANONLY = 1;
    }
    # Perform full run
    else {
      $SYN = 1;
      $PAR = 1;
      $TIMING = 1;
    }
  }
}
# For SIM mode
elsif ($MODE eq "SIM") {
  $TESTLIST = $TESTLIST_OPT;
  # Set other options only if Testlist has not been chosen
  unless ($TESTLIST == 1) {
    $TESTCASE = $TESTCASE_OPT;
    $COM      = $COM_OPT;
    $RUN      = $RUN_OPT;
    $INTER    = $INTER_OPT;
    $NODUMP   = $NODUMP_OPT;
    $WAVE     = $WAVE_OPT;
  }
  # Check if any simulation options have been specified
  if (($COM == 0) && ($RUN == 0) && ($WAVE == 0)) {
    # Clean has been specified so just do a cleanup
    if ($CLEAN == 1) {
      $CLEANONLY = 1;
    }
    # Full run
    else {
      $COM = 1;
      $RUN = 1;
    }
  }
  # Disable batch and interactive switches if run has not been selected
  if ($RUN == 0) {
    $INTER = 0;
  }
}



# Simulation mode
printf ("  Project base directory: \"%s\"\n", convPath($PROJECTBASE));
printf ("  Script startet in %s mode\n", $MODE);

if ($CLEAN == 1) {
  print "    - Cleaning up files\n";
}
if ($DRY == 1) {
  print "    - Dry run only\n";
}

# FPGA options
if ($SYN == 1) {
  print "    - FPGA synthesis\n";
}
if ($PAR == 1) {
  print "    - FPGA place and route\n";
}
if ($TIMING == 1) {
  print "    - FPGA timing analysis\n";
}

# SIM options
if ($COM == 1) {
  print "    - Compiling simulation files\n";
}
#if ($ELAB == 1) {
#  print "    - Starting elaboration\n";
#}
if ($RUN == 1) {
  print "    - Starting ";
  if ($INTER == 1) {
    print "interactive ";
  }
  else {
    print "batch ";
  }
  print "simulation";
  if ($NODUMP == 1) {
    print " (waveform dump disabled)";
  }
  print "\n";
}
if ($WAVE == 1) {
  print "    - Diplaying waveform\n";
}
if ($TESTLIST == 1) {
  print "    - Displaying testcase list\n";
}


# Check if --clean has been specified
if ($CLEAN == 1) {
  # Start clean subroutine
  cleanUp();
  # Exit if no other option has been selected
  if ($CLEANONLY == 1) {
    exit(0);
  }
}

# Check if --testlist has been specified
if ($TESTLIST == 1) {
  displayTestList();
  exit(0);
}


# ------------------------------------------------------------------------------
# Second step: read config file and extract data
print "/-----------------------------------------------------------------------------\\\n";
print "| Reading config file                                                         |\n";
print "\\-----------------------------------------------------------------------------/\n";

#Open config file if it exists, else create new one from template
foreach (@CFGNAME) {
  my $cfgFile = convPath($_);

  if (-f $cfgFile) {
    if ($VERBOSE == 1) {
      printf ("  Reading \"%s\"\n", $cfgFile);
    }
    open(CFGFILE, "<", $cfgFile) or die "Could not open config file!";
    # Read file
    while (defined($line = <CFGFILE>)) {
      $cfgContent .= $line;
    }
    close(CFGFILE);
  }
  # Create new file
  else {
    createConfigFile($cfgFile);
    exit(-1);
  }
}


# Extract data from config file
@IMP_NAME                   = extractConfigInfo("$cfgContent", "implementation_name");
@SOURCE_PATHS               = extractConfigInfo("$cfgContent", "source_paths");
@INCLUDE_PATHS              = extractConfigInfo("$cfgContent", "include_paths");
@VERILOG_DEFINES            = extractConfigInfo("$cfgContent", "verilog_defines");
if ($MODE eq "SIM") {
  @TOOL                     = extractConfigInfo("$cfgContent", "sim_tool");
  @ADDSOURCE_PATHS          = extractConfigInfo("$cfgContent", "sim_source_paths");
  @EXCLUDED_FILES           = extractConfigInfo("$cfgContent", "sim_excluded_files");
  @ADDVERILOG_DEFINES       = extractConfigInfo("$cfgContent", "sim_verilog_defines");
  @TOPLEVEL                 = extractConfigInfo("$cfgContent", "sim_top_level");
  @MSIM_VLOGOPTIONS         = extractConfigInfo("$cfgContent", "modelsim_vlog_options");
  @MSIM_VCOMOPTIONS         = extractConfigInfo("$cfgContent", "modelsim_vcom_options");
  @MSIM_VSIMOPTIONS         = extractConfigInfo("$cfgContent", "modelsim_vsim_options");
  @ICAR_IVERILOGOPTIONS     = extractConfigInfo("$cfgContent", "icarus_iverilog_options");
  @ICAR_VVPOPTIONS          = extractConfigInfo("$cfgContent", "icarus_vvp_options");
  @GHDL_COMOPTIONS          = extractConfigInfo("$cfgContent", "ghdl_compile_options");
  @GHDL_RUNOPTIONS          = extractConfigInfo("$cfgContent", "ghdl_run_options");
  @ISIM_COMPILEOPTIONS      = extractConfigInfo("$cfgContent", "isim_compile_options");
  @ISIM_RUNOPTIONS          = extractConfigInfo("$cfgContent", "isim_run_options");
}
elsif ($MODE eq "FPGA") {
  @TOOL                     = extractConfigInfo("$cfgContent", "fpga_tool");
  @ADDSOURCE_PATHS          = extractConfigInfo("$cfgContent", "fpga_source_paths");
  @EXCLUDED_FILES           = extractConfigInfo("$cfgContent", "fpga_excluded_files");
  @ADDVERILOG_DEFINES       = extractConfigInfo("$cfgContent", "fpga_verilog_defines");
  @TOPLEVEL                 = extractConfigInfo("$cfgContent", "fpga_top_level");
  @FPGAPART                 = extractConfigInfo("$cfgContent", "fpga_part");
  @FPGA_NETLIST_PATHS       = extractConfigInfo("$cfgContent", "fpga_netlist_paths");
  @XIL_XSTOPTIONS           = extractConfigInfo("$cfgContent", "xilinx_xst_options");
  @XIL_NGDBUILDOPTIONS      = extractConfigInfo("$cfgContent", "xilinx_ngdbuild_options");
  @XIL_MAPOPTIONS           = extractConfigInfo("$cfgContent", "xilinx_map_options");
  @XIL_PAROPTIONS           = extractConfigInfo("$cfgContent", "xilinx_par_options");
  @XIL_BITGENOPTIONS        = extractConfigInfo("$cfgContent", "xilinx_bitgen_options");
  @XIL_TRACEOPTIONS         = extractConfigInfo("$cfgContent", "xilinx_trace_options");
  @XIL_SYNCONSTRAINT_FILES  = extractConfigInfo("$cfgContent", "xilinx_syn_constraint_files");
  @XIL_PARCONSTRAINT_FILES  = extractConfigInfo("$cfgContent", "xilinx_par_constraint_files");
  @ALT_CONSTRAINT_FILES     = extractConfigInfo("$cfgContent", "altera_constraint_files");
  @ALT_SDC_FILES            = extractConfigInfo("$cfgContent", "altera_sdc_files");
}

# Append additional sources to regular sources
foreach (@ADDSOURCE_PATHS) {
  push(@SOURCE_PATHS, $_);
}
# Append additional defines to regular defines
foreach (@ADDVERILOG_DEFINES) {
  push(@VERILOG_DEFINES, $_);
}

# Override tool selection
if ($TOOL_OVERRIDE ne '') {
  @TOOL = ($TOOL_OVERRIDE);
}

# Check if only 1 implementation is specified
if (@IMP_NAME != 1) {
  printf ("ERROR: %i implementation names specified.\n", scalar @IMP_NAME);
  foreach (@IMP_NAME) {
    print "  - $_\n";
  }
  exit(-1);
}
elsif ($VERBOSE == 1) {
  print "  Using implementation \"$IMP_NAME[0]\"\n";
}

# Check if only 1 toplevel is specified
if (@TOPLEVEL != 1) {
  printf ("ERROR: %i top level modules specified.\n", scalar @TOPLEVEL);
  foreach (@TOPLEVEL) {
    print "  - $_\n";
  }
  exit(-1);
}
elsif ($VERBOSE == 1) {
  print "  Using top level \"$TOPLEVEL[0]\"\n";
}

# SIM specific report
if ($MODE eq "SIM") {
  # Check if tool has been specified
  if (@TOOL != 1) {
    printf ("ERROR: %i simulation tools specified.\n", scalar @TOOL);
    foreach (@TOOL) {
      print "  - $_\n";
    }
    exit(-1);
  }

  if ($VERBOSE == 1) {
    printf ( "  Using simulation toolchain \"%s\"\n", $TOOL[0]);
  }
}

# FPGA specific report
if ($MODE eq "FPGA") {
  # Check if tool has been specified
  if (@TOOL != 1) {
    printf ("ERROR: %i FPGA tools specified.\n", scalar @TOOL);
    foreach (@TOOL) {
      print "  - $_\n";
    }
    exit(-1);
  }

  # Check if only 1 FPGA part is specified
  if (@FPGAPART != 1) {
    printf ("ERROR: %i FPGA parts specified.\n", scalar @FPGAPART);
    foreach (@FPGAPART) {
      print "  - $_\n";
    }
    exit(-1);
  }

  if ($VERBOSE == 1) {
    printf ("  Using FPGA toolchain \"%s\"\n", $TOOL[0]);
    printf ("  Using FPGA part \"%s\"\n", $FPGAPART[0]);
  }
}


# ------------------------------------------------------------------------------
# Third step: build file list
print "/-----------------------------------------------------------------------------\\\n";
print "| Building source file list and excluding files                               |\n";
print "\\-----------------------------------------------------------------------------/\n";

# Check if entries in array point to file or to directory
foreach $line (@SOURCE_PATHS) {
  # Convert to UNIX format
  if ($line =~ m/\\/) {
    $line = convPathToUnix($line);
  }
  # Expand to absolute path
  $currentEntry = expandToAbsolutePath($line);
  # Check if entry points to directory
  if (-d convPath($currentEntry)) {
    # Open directory, read content and check if it is a file
    opendir(DIR, convPath($currentEntry));
    while ($currentPath = readdir(DIR)) {
      # Expand directory entry with complete path
      $currentPath = $currentEntry."/".$currentPath;
      # Check if it is a file
      if (-f convPath($currentPath)) {
        push(@SOURCEFILE_LIST, cleanPath($currentPath));
      }
    }
    closedir(DIR);
  }

  # Check if entry points to file
  elsif (-f convPath($currentEntry)) {
    push(@SOURCEFILE_LIST, cleanPath($currentEntry));
  }

  # Entry is neither directory nor file
  else {
    printf ("Warning: \"%s\" does not exist\n", convPath($currentEntry));
  }
}

# Step through through list of excluded files
foreach $currentEntry (@EXCLUDED_FILES) {
  # Convert path to UNIX format
  if ($currentEntry =~ m/\\/) {
    $currentEntry = convPathToUnix($currentEntry);
  }
  # Clean up and expand to absolute path
  $currentPath = cleanPath(expandToAbsolutePath($currentEntry));

  # Check if file to exclude exists
  if (!(-f convPath($currentPath))) {
    printf ("Warning: \"%s\" does not exist\n",convPath($currentPath));
  }
  # If it exists check the whole source file list
  else {
    foreach (1..@SOURCEFILE_LIST) {
      # Remove first entry from source file list
      $line = shift(@SOURCEFILE_LIST);
      # If current source file is not a file to exclude put it back at end of source file list
      if (lc($currentPath) ne lc($line)) {
        push(@SOURCEFILE_LIST, $line);
      }
    }
  }
}

# Report
if ($VERBOSE == 1) {
  print "  Source file list:\n";
  foreach (@SOURCEFILE_LIST) {
    printf ("    %s\n",convPath($_));
  }
}



# ------------------------------------------------------------------------------
# Fourth step: check if include paths exist
print "/-----------------------------------------------------------------------------\\\n";
print "| Checking include paths                                                      |\n";
print "\\-----------------------------------------------------------------------------/\n";

foreach (1..@INCLUDE_PATHS) {
  # Get first entry from list
  $line = shift(@INCLUDE_PATHS);
  # Build full UNIX path
  if ($line =~ m/\\/) {
    $line = convPathToUnix($line);
  }
  # Expand to absolute path
  $line = expandToAbsolutePath($line);

  # Check if entry points to directory and put entry with full path back to list, else remove from list
  if (-d convPath($line)) {
    push(@INCLUDE_PATHS, $line);
  }
  else {
    printf ("Warning: \"%s\" is no valid include path\n", convPath($line));
  }
}

# Report
if ($VERBOSE == 1) {
  print "  Include paths:\n";
  foreach (@INCLUDE_PATHS) {
    printf ("    %s\n",convPath($_));
  }
}



foreach (1..@FPGA_NETLIST_PATHS) {
  # Get first entry from list
  $line = shift(@FPGA_NETLIST_PATHS);
  # Build full UNIX path
  if ($line =~ m/\\/) {
    $line = convPathToUnix($line);
  }
  # Expand to absolute path
  $line = expandToAbsolutePath($line);

  # Check if entry points to directory and put entry with full path back to list, else remove from list
  if (-d convPath($line)) {
    push(@FPGA_NETLIST_PATHS, $line);
  }
  else {
    printf ("Warning: \"%s\" is no valid FPGA netlist path\n", convPath($line));
  }
}

# Report
if ($VERBOSE == 1) {
  print "  FPGA netlist paths:\n";
  foreach (@FPGA_NETLIST_PATHS) {
    printf ("    %s\n",convPath($_));
  }
}



# ------------------------------------------------------------------------------
# Fifth step: select between SIM and FPGA run

# --------------------------------------------------------------------------------------------------
# FPGA STUFF
# --------------------------------------------------------------------------------------------------
if ($MODE eq "FPGA") {

  if ($TOOL[0] eq "xilinx") {
    $ret = fpga_xilinx();
  }
  elsif ($TOOL[0] eq "altera") {
    $ret = fpga_altera();
  }
  else {
    printf ("ERROR: FPGA toolchain \"%s\" not supported\n", $TOOL[0]);
    exit(-1);
  }

}

# --------------------------------------------------------------------------------------------------
# SIMULATION STUFF
# --------------------------------------------------------------------------------------------------
elsif ($MODE eq "SIM") {

  if ($TOOL[0] eq "icarus") {
    $ret = sim_icarus();
  }
  elsif ($TOOL[0] eq "modelsim") {
    $ret = sim_modelsim();
  }
  elsif ($TOOL[0] eq "ghdl") {
    $ret = sim_ghdl();
  }
  elsif ($TOOL[0] eq "isim") {
    $ret = sim_isim();
  }
  else {
    printf ("ERROR: simulation toolchain \"%s\" not supported\n", $TOOL[0]);
    exit(-1);
  }

}


exit($ret);














# |-----------------------------------------------------------------------------
# | Perform clean operation
# |-----------------------------------------------------------------------------
sub cleanUp {

  my @simDirs = ("/modelsim",                               # Directories to clean for SIM run
                 "/icarus",
                 "/ghdl",
                 "/isim");
  my @fpgaDirs = ("/xst",                                   # Directories to clean for FPGA run
                  "/ngdbuild",
                  "/map",
                  "/par",
                  "/trace",
                  "/quartus");

  my @dirs;                                                 # Directories to clean
  my $baseDir = $CALLDIR;                                   # Base directory


  # Choose directories for clean
  # FPGA mode
  if ($MODE eq "FPGA") {
    @dirs = @fpgaDirs;
  }
  elsif ($MODE eq "SIM") {
    @dirs = @simDirs;
  }

  print "/-----------------------------------------------------------------------------\\\n";
  print "| Cleaning compile results and intermediate files                             |\n";
  print "\\-----------------------------------------------------------------------------/\n";
  # Step through all FPGA directories and delete all file/directories in them
  foreach (@dirs) {
    # Build path, open directory and read all items
    my $currentDir = $baseDir.$_;
    # Report
    if ($VERBOSE == 1) {
      print "  Cleaning ".convPath($currentDir)."\n";
    }
    if (-d convPath($currentDir)) {
      rmtree($currentDir);
    }
  }
}


# |-----------------------------------------------------------------------------
# | Display a list of all testcases
# |-----------------------------------------------------------------------------
sub displayTestList {
  my $currentDir;
  my $currentItem;
  my $number = 0;

  print "/-----------------------------------------------------------------------------\\\n";
  print "| Displaying testcase list                                                    |\n";
  print "\\-----------------------------------------------------------------------------/\n";

  $currentDir = cleanPath($CALLDIR."/../testcases");

  # Report
  if ($VERBOSE == 1) {
    print "  Testcase directory is \"".convPath($currentDir)."\"\n";
  }

  # Open directory
  opendir(DIR, convPath($currentDir));
  while ($currentItem = readdir(DIR)) {
    # Item is not "." or ".." or something like ".svn" or "_svn" directory
    unless ($currentItem =~ m/^[_\.]/) {
      # Item is directory
      if (-d convPath($currentDir."/".$currentItem)) {
        $number++;
        print "    ".$number.":  \t".$currentItem."\n";
      }
    }
  }
  closedir(DIR);
}










