#!/bin/sh

TEMPDIR=examples_temp
DIR=machet_examples

# Create directory
if [ ! -d "$TEMPDIR" ]; then
    mkdir ./$TEMPDIR
else
    # Check if directory is empty
    if [ "$(ls -A $TEMPDIR)" ]; then
        rm -rf ./$TEMPDIR/*
    fi
fi


# Make a copy of repository
hg archive -r . -t files ./$TEMPDIR
mv ./$TEMPDIR/examples ./$TEMPDIR/$DIR

# Copy readme
#cp ./readme_examples/README ./$TEMPDIR/$DIR

# Copy WTFPL
cp ./license/wtfpl.txt ./$TEMPDIR/$DIR


# Delete old archives
if [ -f "./machet.tar" ]; then
  rm ./machet.tar
fi
if [ -f "./machet.tar.gz" ]; then
  rm ./machet.tar.gz
fi


# Compress
cd $TEMPDIR
tar cvf ./$DIR.tar $DIR/*
gzip ./$DIR.tar
mv ./$DIR.tar.gz ..
cd ..

# Create release directory
if [ ! -d "release" ]; then
    mkdir ./release
fi

# Move archive
if [ -f "./$DIR.tar.gz" ]; then
    mv ./$DIR.tar.gz ./release
fi

# Clean up
rm -rf ./$TEMPDIR
