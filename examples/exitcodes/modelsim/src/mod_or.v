/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */ 


module mod_or
   (a_in,
    b_in,
    a_or_b_out
    );

   input a_in;
   input b_in;
   output a_or_b_out;

   assign a_or_b_out = a_in | b_in;

endmodule