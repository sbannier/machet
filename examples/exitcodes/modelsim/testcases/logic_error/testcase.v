/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */


// machet will exit with code 0, only expected behaviour is wrong, syntax is ok
always @(a_in or b_in) begin
   #1;
   if (a_or_b_out != (a_in & b_in)) begin
      $display("Error!");
      $error(2);
      $finish(2);
   end
end