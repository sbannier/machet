#! /usr/bin/perl -w
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

use strict;

# Select operating system version, standard is non-Windows
my $OS = "X";
if ($^O =~ m/^MSWin/) {
  $OS = "WIN";
}

my @testList = ("no_error", "logic_error", "compile_error_vlib", "compile_error_vlog", "compile_error_vcom", "run_error", "no_testcase");
my @expectedZero = (1, 1, 1, 0, 1, 0, 0);
my @result = ();
my $error = 0;
my $command;


# Run testcase
foreach my $testNr (1..@testList) {
  if ($OS eq "WIN") {
    $command = "machet.pl -N -C ".$testList[$testNr - 1];
  }
  else {
    $command = "machet.pl -N -C ".$testList[$testNr - 1];
  }

  my $ret = system("$command");
  # 8 LSBs of system() return value seem to be exit code of system shell itself
  $result[$testNr - 1] = ($ret >> 8) | ($ret & 0xFF);
}

print "\n\n-------------------------------------------------------------------------------\n";
print "Results:\n\n";
# Evaluate exit codes
foreach my $number (1..@result) {
  print "Testcase $number (".$testList[$number - 1]."): Expected ";
  if ($expectedZero[$number - 1]) {
    print "0 ";
  }
  else {
    print "non-zero value ";
  }

  print "as exit code, got ".$result[$number - 1];

  if ((!$expectedZero[$number - 1] && !$result[$number - 1]) ||
      ($expectedZero[$number - 1] && $result[$number - 1])) {
    print "\n   ERROR, see line above for description!";
    $error = 1;
  }

  print "\n";
}

if ($error == 1) {
  print "\nTest failed!\n";
print "-------------------------------------------------------------------------------\n";
  exit(100);
}
print "-------------------------------------------------------------------------------\n";
exit(0);
