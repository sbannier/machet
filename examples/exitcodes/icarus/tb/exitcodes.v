/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */


module exitcodes_icarus
   ();
   
   reg a_in;
   reg b_in;
   wire a_out;

   mod_or dut_mod_or
      (
       .a_in           (a_in),
       .b_in           (b_in),
       .a_or_b_out     (a_or_b_out)
       );

   initial begin
      #10;
      $display("--------");
      $display("a=0, b=0");
      a_in <= 1'b0;
      b_in <= 1'b0;
      
      #10;
      $display("--------");
      $display("a=1, b=0");
      a_in <= 1'b1;
      b_in <= 1'b0;

      #10;
      $display("--------");
      $display("a=0, b=1");
      a_in <= 1'b0;
      b_in <= 1'b1;

      #10;
      $display("--------");
      $display("a=1, b=1");
      a_in <= 1'b1;
      b_in <= 1'b1;
      #10;
   end
      
`include "testcase.v"
    
endmodule