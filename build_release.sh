#!/bin/sh

DIR=machet

# Create directory
if [ ! -d "$DIR" ]; then
    mkdir ./$DIR
else
    # Check if directory is empty
    if [ "$(ls -A $DIR)" ]; then
        rm -r ./$DIR/*
    fi
fi


# Copy source files
cp ./source/machet_cfg_utils.pl    ./$DIR
cp ./source/machet_config.pl       ./$DIR
cp ./source/machet_fpga_altera.pl  ./$DIR
cp ./source/machet_fpga_xilinx.pl  ./$DIR
cp ./source/machet_help.pl         ./$DIR
cp ./source/machet.pl              ./$DIR
cp ./source/machet_sim_ghdl.pl     ./$DIR
cp ./source/machet_sim_icarus.pl   ./$DIR
cp ./source/machet_sim_isim.pl     ./$DIR
cp ./source/machet_sim_modelsim.pl ./$DIR
cp ./source/machet_utils.pl        ./$DIR

# Copy readme
cp ./readme/README ./$DIR

# Copy GPL
cp ./license/gpl.txt ./$DIR


# Delete old archives
if [ -f "./machet.tar" ]; then
  rm ./machet.tar
fi
if [ -f "./machet.tar.gz" ]; then
  rm ./machet.tar.gz
fi


# Compress
tar cvf ./machet.tar $DIR/*
gzip ./machet.tar


# Create release directory
if [ ! -d "release" ]; then
    mkdir ./release
fi

# Move archive
if [ -f "./machet.tar.gz" ]; then
    mv ./machet.tar.gz ./release
fi

# Clean up
rm -r ./$DIR/*
rmdir ./$DIR
